﻿/*
 * Author : Fionn Mahnig
 * Class purpose: Inherits from Guest, holds details for the customers
 * Date last modified: 09/12/2016 
 * Part of: Facade and Factory design patterns
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace CourseWork2017
{
    class Customer : Guest
    {

        private string custAddress; //address
        private int customerRefNum; //customer reference number

        #region ACCESSORS
        /// <summary>
        /// accessor customer ref number
        /// </summary>
        public int CustomerRefNum
        {
            get { return customerRefNum; }
            set { customerRefNum = value; }
        }

        /// <summary>
        /// customer's address
        /// </summary>
        public string Address
        {
            get { return custAddress; }
            set
            {
                //if the value in the box is empty
                if (value == "")
                {
                    //throw an error message
                    throw new ArgumentException("Last Name, First Name and Address need to be filled in.");                    
                }
                //if value is valid
                custAddress = value;
            }
        }

        #endregion

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="type">type (customer)</param>
        /// <param name="custRefNum">customer ref num</param>
        /// <param name="fName">first name</param>
        /// <param name="lName">last name</param>
        /// <param name="address"> address</param>
        /// <param name="passportNum">passport number</param>
        /// <param name="age">age</param>
        public Customer(string type, int custRefNum, int bookingNumber, string fName, string lName, string address, string passportNum, int age)
            : base(type, bookingNumber, fName, lName, passportNum, age)
        {
            base.Type = type;
            customerRefNum = custRefNum;
            base.firstName = fName;
            base.lastName = lName;
            this.custAddress = address;
            base.guestAge = age;
            base.passportNum = passportNum;
        }

        public override void print()
        {
        }
    }
}
