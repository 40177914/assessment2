﻿/*
 * Author : Fionn Mahnig
 * Class purpose: Base class to set up extras and be able to save them, add them to a booking,...
 * Date last modified: 09/12/2016 
 * Part of: Facade design pattern
 * Assumed that Extras will need a booking number to be linked to a booking and to make it easier to find them
*/


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourseWork2017
{
    class Extras
    {
        //what needs to be saved

        //booking number
        private int bookingNum;
        //breakfast diet requirements
        private string breakDietRequirements;
        //evening meal diet requirements
        private string eveDietRequirements;
        //name of the driver
        private string nameDriver;
        //car hire start date and end date
        private DateTime hireStartDate, hireEndDate;

        #region accessors
        public int BookingNum
        {
            get { return bookingNum; }
            set { bookingNum = value; }
        }

        public string BreakDietRequirements
        {
            get { return breakDietRequirements; }
            set { breakDietRequirements = value; }
        }

        public string EveDietRequirements
        {
            get { return eveDietRequirements; }
            set { eveDietRequirements = value; }
        }

        public string NameDriver
        {
            get { return nameDriver; }
            set { nameDriver = value; }
        }

        public DateTime HireStartDate
        {
            get { return hireStartDate; }
            set { hireStartDate = value; }
        }

        public DateTime HireEndDate
        {
            get { return hireEndDate; }
            set { hireEndDate = value; }
        }

        #endregion


        /// <summary>
        /// Constructor. Booking num is mandatory while all the others are optional as not everybody needs all the extras
        /// </summary>
        /// <param name="bookingNum">booking number</param>
        /// <param name="breakDietReq">breakfast diet requirement</param>
        /// <param name="eveDietReq">evening meals diet requirement</param>
        /// <param name="driverName">driver name</param>
        /// <param name="hireStart">vehicle hire start date</param>
        /// <param name="hireEnd">vehicle hire end date</param>
        public Extras(int bookingNum, string breakDietReq = "", string eveDietReq = "", string driverName ="", DateTime hireStart = default(DateTime), DateTime hireEnd = default(DateTime))
        {
            this.bookingNum = bookingNum;
            breakDietRequirements = breakDietReq;
            eveDietRequirements = eveDietReq;
            nameDriver = driverName;
            hireStartDate = hireStart;
            hireEndDate = hireEnd;
        }
    }
}
