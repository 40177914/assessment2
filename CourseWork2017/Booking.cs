﻿/*
 * Author : Fionn Mahnig
 * Class purpose: Class to hold the details of bookings
 * Date last modified: 09/12/2016 
 * Part of: Facade and Factory design patterns
*/


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourseWork2017
{
    class Booking
    {
        //different details
        //booking ref number
        private int bookingRefNumber;
        //customer ref number
        private int customerRefNumber;
        //arrival / departure date
        private DateTime arrivalDate;
        private DateTime departureDate;

        #region accessors
        public int CustomerRefNumber
        {
            get { return customerRefNumber; }
            set { customerRefNumber = value; }
        }

        public int BookingRefNumber
        {
            get { return bookingRefNumber; }
            set { bookingRefNumber = value; }
        }


        public DateTime ArrivalDate
        {
            get { return arrivalDate; }
            set { arrivalDate = value; }
        }

        public DateTime DepartureDate
        {
            get { return departureDate; }
            set { departureDate = value; }
        }

        #endregion

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="bookingRef">booking ref number</param>
        /// <param name="customerRefNum">customer ref number</param>
        /// <param name="arrDate">arrival date</param>
        /// <param name="depDate">departure date</param>
        public Booking(int bookingRef, int customerRefNum, DateTime arrDate, DateTime depDate)
        {
            customerRefNumber = customerRefNum;
            bookingRefNumber = bookingRef;
            arrivalDate = arrDate;
            departureDate = depDate;
        }

    }
}
