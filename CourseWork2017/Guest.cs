﻿/*
 * Author : Fionn Mahnig
 * Class purpose: Inherits from Person, holds details for the guests
 * Date last modified: 09/12/2016 
 * Part of: Factory and facade design pattern
 * Assumed that a guest will need a booking number so as to make it easier to find and to link it to a booking
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace CourseWork2017
{
    class Guest : Person
    {
        //varibales to hold the data for the guests

        //passport number
        protected string passportNum;
        //guest's age
        protected int guestAge;
        //booking number that the guest is associated to
        private int bookingNumber;

        #region accessors
        public int BookingNumber
        {
            get { return bookingNumber; }
            set { bookingNumber = value; }
        }

        //age of the guest accessor
        public int Age
        {
            get { return guestAge; }
            set { guestAge = value; }
        }

        //pasport number accessor, mandatory. If no value, messagebox to show an error
        public string PassportNum
        {
            get { return passportNum; }
            set
            {
                if (value == "")
                {
                    throw new ArgumentException("Passport Number and age need to be filled in.");
                }
                passportNum = value;
            }
        }

        #endregion

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="type">type (guest)</param>
        /// <param name="fName">first name</param>
        /// <param name="lName">last name</param>
        /// <param name="passNum"> passport number</param>
        /// <param name="age">age </param>
        public Guest(string type, int bookingNum, string fName, string lName, string passNum, int age)
        {
            //type, first name and last name come from Person
            base.Type = type;
            bookingNumber = bookingNum;
            base.firstName = fName;
            base.lastName = lName;
            this.passportNum = passNum;
            this.guestAge = age;
        }
        public override void print()
        {
            throw new NotImplementedException();
        }
    }
}
