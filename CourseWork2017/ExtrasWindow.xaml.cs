﻿/*
 * Author : Fionn Mahnig
 * Class purpose: Class that holds the code for the GUI of the extras window 
 * Date last modified: 09/12/2016 
 * Part of: Facade design pattern
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CourseWork2017
{
    /// <summary>
    /// Interaction logic for ExtrasWindow.xaml
    /// </summary>
    public partial class ExtrasWindow : Window
    {
        //reference to the facade
        private Facade facade = new Facade();
        //reference to the logger
        private Logger logger = Logger.Instance;

        public ExtrasWindow()
        {
            InitializeComponent();
        }

        #region ENABLE LABELS AND TEXT BOXES + CHECK IF CHECKED

        /// <summary>
        /// Enable the labels and textbox for evening meals when checked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void chkEveMeal_Click(object sender, RoutedEventArgs e)
        {
            //if the lbl and textbox are not enabled
            if (!lblEveMealDietaryRequirements.IsEnabled && !txtEveMealDietReq.IsEnabled)
            {               
                //enable them on click
                lblEveMealDietaryRequirements.IsEnabled = true;
                txtEveMealDietReq.IsEnabled = true;
            }
            else
            {
                //else disable them
                lblEveMealDietaryRequirements.IsEnabled = false;
                txtEveMealDietReq.IsEnabled = false;
            }
        }


        /// <summary>
        ///  Enable the labels and textbox for breakfst when checked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void chkBreakfast_Click(object sender, RoutedEventArgs e)
        {
            //if the lbl and textbox are not enabled
            if (lblBreakDietaryRequirements.IsEnabled == false && txtBreakDietReq.IsEnabled == false)
            {
                //enable them on click
                lblBreakDietaryRequirements.IsEnabled = true;
                txtBreakDietReq.IsEnabled = true;
            }
            else
            {
                //else disable them
                lblBreakDietaryRequirements.IsEnabled = false;
                txtBreakDietReq.IsEnabled = false;
            }
        }
        /// <summary>
        /// what happens when you click on the car hire check box (enable/disable boxes and labels)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void chkCarHire_Click(object sender, RoutedEventArgs e)
        {
            //if the labels are not enabled
            if (!lblHireStart.IsEnabled)
            {
                //enable them
                lblHireEnd.IsEnabled = true;
                lblHireStart.IsEnabled = true;
                lblNameDriver.IsEnabled = true;
                txtExtrasDriverName.IsEnabled = true;
                datHireEnd.IsEnabled = true;
                datHireStart.IsEnabled = true;
            }
            else
            { 
                //else disable them
                lblNameDriver.IsEnabled = false;
                lblHireStart.IsEnabled = false;
                lblHireEnd.IsEnabled = false;
                txtExtrasDriverName.IsEnabled = false;
                datHireEnd.IsEnabled = false;
                datHireStart.IsEnabled = false;
            }
        }

        #endregion

        #region PRESS SAVE BUTTON
        /// <summary>
        /// what happens when you press the save button on the extras window and depending on what is checked and filled in
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnExtrasSave_Click(object sender, RoutedEventArgs e)
        {
            //if all three extras are checked, save all the necessary data
            if (chkBreakfast.IsChecked == true && chkCarHire.IsChecked == true && chkEveMeal.IsChecked == true)
            {
                //add a new extra to the list with the necessary data
                facade.AddNewExtras(Convert.ToInt32(txtExtrasBookingNum.Text), txtBreakDietReq.Text, txtEveMealDietReq.Text, txtExtrasDriverName.Text,datHireStart.SelectedDate.Value.Date, datHireEnd.SelectedDate.Value.Date);
                logger.logExtras(Convert.ToInt32(txtExtrasBookingNum.Text), txtBreakDietReq.Text, txtEveMealDietReq.Text, datHireStart.SelectedDate.Value.Date, datHireEnd.SelectedDate.Value.Date, txtExtrasDriverName.Text);
                this.Close();
            }
                //else if only the 2 first checkboxes are checked, save necessary data
            else if (chkBreakfast.IsChecked == true && chkEveMeal.IsChecked == true && chkCarHire.IsChecked == false)
            {
                facade.AddNewExtras(Convert.ToInt32(txtExtrasBookingNum.Text), txtBreakDietReq.Text, txtEveMealDietReq.Text);
                logger.logExtras(Convert.ToInt32(txtExtrasBookingNum.Text), txtBreakDietReq.Text, txtEveMealDietReq.Text);
                this.Close();
            }
                //if only bbreakfast checked
            else if (chkBreakfast.IsChecked == true && chkEveMeal.IsChecked == false && chkCarHire.IsChecked == false)
            {
                //add only breakfast to the list and log
                facade.AddNewExtras(Convert.ToInt32(txtExtrasBookingNum.Text), txtBreakDietReq.Text);
                logger.logExtras(Convert.ToInt32(txtExtrasBookingNum.Text), txtBreakDietReq.Text);
                //close the extras window
                this.Close();
            }
                //if only eve meal checked 
            else if (chkBreakfast.IsChecked == false && chkEveMeal.IsChecked == true && chkCarHire.IsChecked == false)
            {
                //add only eve meal to list and log
                facade.AddNewExtras(Convert.ToInt32(txtExtrasBookingNum.Text), eveDietReq: txtEveMealDietReq.Text);
                logger.logExtras(Convert.ToInt32(txtExtrasBookingNum.Text), breakRequirement: "", eveRequirements: txtEveMealDietReq.Text);
                //close the window
                this.Close();
            }
                //if only car hire is checked
            else if (chkBreakfast.IsChecked == false && chkEveMeal.IsChecked == false && chkCarHire.IsChecked == true)
            {
                //add only car hire to list and log
                facade.AddNewExtras(Convert.ToInt32(txtExtrasBookingNum.Text), driverName: txtExtrasDriverName.Text, hireStart: datHireStart.SelectedDate.Value.Date, hireEnd: datHireEnd.SelectedDate.Value.Date);
                logger.logExtras(Convert.ToInt32(txtExtrasBookingNum.Text), driverName: txtExtrasDriverName.Text, hireDate: datHireStart.SelectedDate.Value.Date, endDate: datHireEnd.SelectedDate.Value.Date);
                //close the window
                this.Close();
            }
            //if only car hire and breakfast is checked 
            else if (chkBreakfast.IsChecked == true && chkEveMeal.IsChecked == false && chkCarHire.IsChecked == true)
            {
                //add only car hire and breakfasts to list and log
                facade.AddNewExtras(Convert.ToInt32(txtExtrasBookingNum.Text),breakDietReq:txtBreakDietReq.Text,  driverName: txtExtrasDriverName.Text, hireStart: datHireStart.SelectedDate.Value.Date, hireEnd: datHireEnd.SelectedDate.Value.Date);
                logger.logExtras(Convert.ToInt32(txtExtrasBookingNum.Text), breakRequirement: txtBreakDietReq.Text, driverName: txtExtrasDriverName.Text, hireDate: datHireStart.SelectedDate.Value.Date, endDate: datHireEnd.SelectedDate.Value.Date);
                //close the window
                this.Close();
            }
            //if only car hire and eve meal is checked 
            else if (chkBreakfast.IsChecked == false && chkEveMeal.IsChecked == true && chkCarHire.IsChecked == true)
            {
                //add only car hire and eve meal to list and log
                facade.AddNewExtras(Convert.ToInt32(txtExtrasBookingNum.Text), eveDietReq: txtEveMealDietReq.Text, driverName: txtExtrasDriverName.Text, hireStart: datHireStart.SelectedDate.Value.Date, hireEnd: datHireEnd.SelectedDate.Value.Date);
                logger.logExtras(Convert.ToInt32(txtExtrasBookingNum.Text), eveRequirements: txtEveMealDietReq.Text, driverName: txtExtrasDriverName.Text, hireDate: datHireStart.SelectedDate.Value.Date, endDate: datHireEnd.SelectedDate.Value.Date);
                //close the window
                this.Close();
            }
        }
        #endregion
    }
}
