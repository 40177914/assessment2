﻿/*
 * Author : Fionn Mahnig
 * Class purpose: This class saves the different customers/guests/bookings/extras to the csv files and also has the methods
 * to amend customers/bookings/extras/guests and delete customer and bookings
 * Date last modified: 09/12/2016 
 * Part of: Factory and facade design pattern
*/

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace CourseWork2017
{
    public class Logger
    {
        private Collections lists = Collections.Instance;
        // This holds a refence to the ONLY Logger object
        private static Logger instance;
        //constructor
        private Logger()
        { }

        /* This property is used to access the only instance of Logger. It is static in
            it to be called without instanciating an object.
            It returns an object that represents the only instance of the Logger class*/
        public static Logger Instance
        {          
            get
            {
                //If this is being called for the first time, instanciate a Logger object.       
                if (instance == null)
                {
                    instance = new Logger();
                }
                return instance;
            }
        }

        #region LOG CUSTOMERS // GUESTS // BOOKINGS // EXTRAS to file

        /// <summary>
        /// log the customers to a file
        /// </summary>
        /// <param name="customerRefNumber">Customer reference number  to be logged</param>
        /// <param name="customerName"> customer name to be logged</param>
        /// <param name="customerAddress">customer address to be logged</param>
        public void logCustomer(int customerRefNumber, string customerFName, string customerLName, string customerAddress)
        {
            
            //if the file doesn't exist, create it and add on the first line the different headers
            if (new FileInfo("LogCustomers.csv").Exists == false)
            {
                //append headers to the top of the file, first line
                File.AppendAllText("LogCustomers.csv", "Customer Ref. Number, " + "First Name," + "Last Name," + "Address" + "\n");
            }
            
            //append the details received from the factory in the csv file        
            File.AppendAllText("LogCustomers.csv",  customerRefNumber + ","+ customerFName + ","  + customerLName + "," + customerAddress + "\n");
        }

        /// <summary>
        /// log the guests to a file
        /// </summary>
        /// <param name="bookingNum">guest's booking number to be logged</param>
        /// <param name="guestName">guest's name to be logged</param>
        /// <param name="passportNumber">guest's passport num  to be logged</param>
        /// <param name="age">guest's age  to be logged</param>
        public void logGuest(int bookingNum, string guestFName, string guestLName,  string passportNumber, int age)
        {
            //if the file doesn't exist, create it and add on the first line the different headers
            if (new FileInfo("LogGuests.csv").Exists == false)
            {
                //append headers to the top of the file, first line
                File.AppendAllText("LogGuests.csv", "Booking Number," + "First Name," + "Last Name,"+ "Passport Number," + "Age" + "\n");
            }

            //append the details received from the factory in the csv file        
           File.AppendAllText("LogGuests.csv", bookingNum + "," + guestFName + ","  + guestLName + "," + passportNumber + "," + age + "\n");

        }

        public void logBookings(int bookingNum, int custRefNum, DateTime arrivalDate, DateTime departureDate)
        {
            //if the file doesn't exist, create it and add on the first line the different headers
            if (new FileInfo("LogBookings.csv").Exists == false)
            {
                //append headers to the top of the file, first line
                File.AppendAllText("LogBookings.csv", "Booking Number," + "Customer Ref Number," + "Arrival Date," + "Departure Date," + "\n");
            }

            //append the details received from the factory in the csv file        
            File.AppendAllText("LogBookings.csv", bookingNum + ", " + custRefNum + ", "+ arrivalDate + ", " + departureDate + "\n");
        }

        public void logExtras(int bookingNum, string breakRequirement = "", string eveRequirements = "",  DateTime hireDate =default(DateTime),  DateTime endDate = default(DateTime), string driverName = "")
        {
            //if the file doesn't exist, create it and add on the first line the different headers
            if (new FileInfo("LogExtras.csv").Exists == false)
            {
                //append headers to the top of the file, first line
                File.AppendAllText("LogExtras.csv", "Booking Number," + "Breakfast requirements," + "Evening meals requirements," + "Car hire date start," + "Car Hire Date end," + "Driver name," + "\n");
            }

            //append the details received from the factory in the csv file        
            File.AppendAllText("LogExtras.csv", bookingNum + ", " + breakRequirement + ", " + eveRequirements + ", " + hireDate + ", " + endDate + ", " + driverName + "\n" );
        }

        #endregion

        #region AMEND CUSTOMERS

        /// <summary>
        /// Method that will allow to amend the customer details from the amend customer tab
        /// </summary>
        /// <param name="custRefNumber">The customer reference number passed</param>
        /// <param name="firstName">The new first name</param>
        /// <param name="lastName">the new last name</param>
        /// <param name="address">the new address</param>
        public void AmendCustomers( int custRefNumber, string firstName, string lastName, string address)
        {
            //source path of the file, depending on the type       
            var sourcePath = "LogCustomers.csv";      
            //does the first line con
            var firstLineContainsHeaders = true;
            //delimiter
            var delimiter = ",";
            //temporary path of the new file
            var tempPath = System.IO.Path.GetTempFileName();
            //using a streeam writer and a stream reader
            using (var writer = new StreamWriter(tempPath))
            using (var reader = new StreamReader(sourcePath))
            {
                //create a line string
                string line = null;
                //create an array of strings that will contain the separated values
                string[] headers = null;

                //if the first line contains a header, write it in the new file first of all
                if (firstLineContainsHeaders)
                {
                    //read the file
                    line = reader.ReadLine();
                    //split the line
                    headers = line.Split(',');
                    //write the headers line
                    writer.WriteLine(line);
                }

                //while the reader is not at the end of the file 
                while ((line = reader.ReadLine()) != null)
                {
                    //split the lines 
                    var columns = line.Split(',');
                    //if the column 0 equals the customer ref num
                    if (columns[0] == custRefNumber.ToString())
                        {
                            //replace the different values with the values that have been entered in the amend window
                            columns[1] = columns[1].Replace(columns[1], firstName);
                            columns[2] = columns[2].Replace(columns[2], lastName);
                            columns[3] = columns[3].Replace(columns[3], address);
                        
                        //update the list as well
                            foreach (Customer c in lists.CustomersList)
                            {
                                //if column 0 equals a customer reference number in the list
                                if (columns[0] == c.CustomerRefNum.ToString())
                                {
                                    //modify the details of the customer associated with that number
                                    c.FirstName = firstName;
                                    c.LastName = lastName;
                                    c.Address = address;
                                }
                            }

                            //show a message saying showing the changes made
                            MessageBox.Show("Success! New Details: " + firstName + " " + lastName + ", " + address + ".");
                        }                 
                    //put the elements back together
                    writer.WriteLine(string.Join(delimiter, columns));
                }
            }
            //delete the old file 
            File.Delete(sourcePath);
            //move the new file from the temp path to the original path
            File.Move(tempPath, sourcePath);
        }
        #endregion

        #region AMEND GUESTS
        /// <summary>
        /// Method that will allow to amend the guests details from the amend guests tab
        /// </summary>
        /// <param name="bookingNum">booking number associated with the guest</param>
        /// <param name="firstName">guest's first name</param>
        /// <param name="lastName">guest's last name</param>
        /// <param name="passportNum">guest's passport number</param>
        /// <param name="age">guest's age</param>
        public void AmendGuests(int bookingNum, string firstName, string lastName, string passportNum, int age)
        {
            //source path of the file, depending on the type       
            var sourcePath = "LogGuests.csv";
            //does the first line con
            var firstLineContainsHeaders = true;
            //delimiter
            var delimiter = ",";
            //temporary path of the new file
            var tempPath = System.IO.Path.GetTempFileName();
            //using a streeam writer and a stream reader
            using (var writer = new StreamWriter(tempPath))
            using (var reader = new StreamReader(sourcePath))
            {
                //create a line string
                string line = null;
                //create an array of strings that will contain the separated values
                string[] headers = null;

                //if the first line contains a header write it in the new file first of all
                if (firstLineContainsHeaders)
                {
                    //read the file
                    line = reader.ReadLine();
                    //split the line
                    headers = line.Split(',');
                    //write the headers line
                    writer.WriteLine(line);
                }

                //while the reader is not at the end of the file 
                while ((line = reader.ReadLine()) != null)
                {
                    //split the lines 
                    var columns = line.Split(',');
                    //check if it is the same guest by checking the booking number, first name and last name
                    if (columns[0] == bookingNum.ToString() && columns[1] == firstName && columns[2] == lastName)
                    {
                        //replace the different values with the values that have been entered in the amend window
                        columns[1] = columns[1].Replace(columns[1], firstName);
                        columns[2] = columns[2].Replace(columns[2], lastName);
                        columns[3] = columns[3].Replace(columns[3], passportNum);
                        columns[4] = columns[4].Replace(columns[4], age.ToString());

                        //update the list as well
                        foreach (Guest g in lists.GuestsList)
                        {
                            //if column 0 equals a customer reference number in the list
                            if (columns[0] == g.BookingNumber.ToString() && g.FirstName == firstName && g.LastName == lastName)
                            {
                                //modify the details of the customer associated with that number
                                g.FirstName = firstName;
                                g.LastName = lastName;
                                g.PassportNum = passportNum;
                                g.Age = age;
                            }
                        }

                        //show a message saying showing the changes made
                        MessageBox.Show("New Details: " + firstName + " " + lastName + ", " + passportNum + ", " + age + ".");
                    }
                    //put the elements back together
                    writer.WriteLine(string.Join(delimiter, columns));
                }
            }
            //delete the old file 
            File.Delete(sourcePath);
            //move the new file from the temp path to the original path
            File.Move(tempPath, sourcePath);
        }
        #endregion 

        #region AMEND BOOKINGS

        /// <summary>
        /// Method that will allow to amend the bookings details from the amend bookings tab
        /// </summary>
        /// <param name="bookingNum">booking number searched for</param>
        /// <param name="arrivalDate">arrival date to be amended</param>
        /// <param name="departureDate">departure date to be amended</param>
        public void AmendBookings(int bookingNum, DateTime arrivalDate, DateTime departureDate)
        {
            //source path of the file, depending on the type       
            var sourcePath = "LogBookings.csv";
            //does the first line con
            var firstLineContainsHeaders = true;
            //delimiter
            var delimiter = ",";
            //temporary path of the new file
            var tempPath = System.IO.Path.GetTempFileName();        
            //using a streeam writer and a stream reader
            using (var writer = new StreamWriter(tempPath))
            using (var reader = new StreamReader(sourcePath))
            {
                //create a line string
                string line = null;
                //create an array of strings that will contain the separated values
                string[] headers = null;

                //if the first line contains a header write it in the new file first of all
                if (firstLineContainsHeaders)
                {
                    //read the file
                    line = reader.ReadLine();

                    //split the line
                    headers = line.Split(',');
                    //write the headers line
                    writer.WriteLine(line);
                }

                //while the reader is not at the end of the file 
                while ((line = reader.ReadLine()) != null)
                {
   
                    //split the lines 
                    var columns = line.Split(',');
                    //check if it is the same guest by checking the booking number, first name and last name
                    if (columns[0] == bookingNum.ToString() )
                    {
                        //replace the different values with the values that have been entered in the amend window
                        columns[2] = columns[2].Replace(columns[2], arrivalDate.ToString());
                        columns[3] = columns[3].Replace(columns[3], departureDate.ToString());
                        
                        //update the list as well
                        foreach (Booking b in lists.BookingsList)
                        {
                            //if column 0 equals a customer reference number in the list
                            if (columns[0] == b.BookingRefNumber.ToString() )
                            {
                                //modify the details of the customer associated with that number
                                b.ArrivalDate = arrivalDate;
                                b.DepartureDate = departureDate;
                            }
                        }

                        //show a message saying showing the changes made
                        MessageBox.Show("New Details: Arrival date: " + arrivalDate.ToShortDateString() + ", departure date: " + departureDate.ToShortDateString() + ".");
                    }
                    //put the elements back together
                    writer.WriteLine(string.Join(delimiter, columns));
                }
            }
            //delete the old file 
            File.Delete(sourcePath);
            //move the new file from the temp path to the original path
            File.Move(tempPath, sourcePath);
        }
        #endregion

        #region AMEND EXTRAS
        /// <summary>
        /// Method that will allow to amend the extras details from the amend extras tab
        /// </summary>
        /// <param name="bookingNumber">booking number to find the extras</param>
        /// <param name="breakfastRequirements">breakfast requirements to be amended</param>
        /// <param name="eveningMealRequirements">evening meal requirements to be amended</param>
        /// <param name="carHireDate">care hire start date to be amended</param>
        /// <param name="carHireEndDate">care hire end date to be amended</param>
        /// <param name="driverName">driver name to be amended</param>
        public void AmendExtras(int bookingNumber, string breakfastRequirements, string eveningMealRequirements, DateTime carHireDate, DateTime carHireEndDate, string driverName)
        {
            //source path of the file, depending on the type       
            var sourcePath = "LogExtras.csv";
            //does the first line con
            var firstLineContainsHeaders = true;
            //delimiter
            var delimiter = ",";
            //temporary path of the new file
            var tempPath = System.IO.Path.GetTempFileName();
            //using a streeam writer and a stream reader
            using (var writer = new StreamWriter(tempPath))
            using (var reader = new StreamReader(sourcePath))
            {
                //create a line string
                string line = null;
                //create an array of strings that will contain the separated values
                string[] headers = null;

                //if the first line contains a header write it in the new file first of all
                if (firstLineContainsHeaders)
                {
                    //read the file
                    line = reader.ReadLine();
                    //split the line
                    headers = line.Split(',');
                    //write the headers line
                    writer.WriteLine(line);
                }

                //while the reader is not at the end of the file 
                while ((line = reader.ReadLine()) != null)
                {
                    //split the lines 
                    var columns = line.Split(',');
                    //check if it is the same guest by checking the booking number, first name and last name
                    if (columns[0] == bookingNumber.ToString())
                    {
                        //replace the different values with the values that have been entered in the amend window
                        columns[1] = columns[1].Replace(columns[1], breakfastRequirements);
                        columns[2] = columns[2].Replace(columns[2], eveningMealRequirements);
                        columns[3] = columns[3].Replace(columns[3], carHireDate.ToString());
                        columns[4] = columns[4].Replace(columns[4], carHireEndDate.ToString());
                        columns[5] = columns[5].Replace(columns[5], driverName);

                        //update the list as well
                        foreach (Extras e in lists.ExtrasList)
                        {
                            //if column 0 equals a customer reference number in the list
                            if (columns[0] == e.BookingNum.ToString())
                            {
                                //modify the details of the customer associated with that number
                                e.BreakDietRequirements = breakfastRequirements;
                                e.EveDietRequirements = eveningMealRequirements;
                                e.HireStartDate = carHireDate;
                                e.HireEndDate = carHireEndDate;
                                e.NameDriver = driverName;
                            }
                        }

                        //show a message saying showing the changes made
                        MessageBox.Show("New Details: Breakfast: " + breakfastRequirements + ", Evening Meal: " + eveningMealRequirements + ", Car hire start date: " 
                            + carHireDate.ToShortDateString() + ", Car hire end date: "+ carHireEndDate.ToShortDateString() + ", Driver name: " + driverName +  ".");
                    }
                    //put the elements back together
                    writer.WriteLine(string.Join(delimiter, columns));
                }
            }
            //delete the old file 
            File.Delete(sourcePath);
            //move the new file from the temp path to the original path
            File.Move(tempPath, sourcePath);
        }
        #endregion

        #region DELETE BOOKINGS
        /// <summary>
        /// Delete a booking from the csv file and from the list
        /// </summary>
        /// <param name="bookingNumber">booking number to be deleted</param>
        public void DeleteBooking(int bookingNumber)           
        {
            //source path of the file, depending on the type       
            var sourcePath = "LogBookings.csv";
            //does the first line con
            var firstLineContainsHeaders = true;
            //delimiter
            var delimiter = ",";
            //temporary path of the new file
            var tempPath = System.IO.Path.GetTempFileName();
            //using a streeam writer and a stream reader
            using (var writer = new StreamWriter(tempPath))
            using (var reader = new StreamReader(sourcePath))
            {
                //create a line string
                string line = null;
                //create an array of strings that will contain the separated values
                string[] headers = null;

                //if the first line contains a header, write it to the file straight away so that it doesn't get read
                if (firstLineContainsHeaders)
                {
                    //read the file
                    line = reader.ReadLine();                   
                    //split the line
                    headers = line.Split(',');
                    //write the headers line
                    writer.WriteLine(line);
                }

                //while the reader is not at the end of the file 
                while ((line = reader.ReadLine()) != null)
                {                    
                    //split the lines 
                    var columns = line.Split(',');
                    //if what is in the first column of the split line isn't equal to the booking number being looked for
                    if (columns[0] != bookingNumber.ToString())
                    {                       
                        //write the line to the temp file
                        writer.WriteLine(string.Join(delimiter, columns));                    
                    }
                    else
                    {                        
                        //show a message showing the changes made
                        MessageBox.Show("Deleted: booking " + bookingNumber);

                        //update the list as well      
                        foreach (Booking b in lists.BookingsList)
                        {
                            //if the booking number given equals a customer reference number in the list
                            if (bookingNumber == b.BookingRefNumber)
                            {
                                //remove the booking from the listr 
                                lists.BookingsList.Remove(b);
                                break;
                            }
                        }  
                    }                
                }
            }
            //delete the old file 
            File.Delete(sourcePath);
            //move the new file from the temp path to the original path
            File.Move(tempPath, sourcePath);

        }
        #endregion

        #region DELETE CUSTOMER
        /// <summary>
        /// Delete a customer from the csv file and from the list by using the number given
        /// </summary>
        /// <param name="customerRefNum">customer reference number given that needs deleted</param>
        public void DeleteCustomer(int customerRefNum)
        {
            //source path of the file, depending on the type       
            var sourcePath = "LogCustomers.csv";
            //does the first line con
            var firstLineContainsHeaders = true;
            //delimiter
            var delimiter = ",";
            //temporary path of the new file
            var tempPath = System.IO.Path.GetTempFileName();
            //using a streeam writer and a stream reader
            using (var writer = new StreamWriter(tempPath))
            using (var reader = new StreamReader(sourcePath))
            {
                //create a line string
                string line = null;
                //create an array of strings that will contain the separated values
                string[] headers = null;

                //if the first line contains a header, write it to the file straight away so that it doesn't get read
                if (firstLineContainsHeaders)
                {
                    //read the file
                    line = reader.ReadLine();
                    //split the line
                    headers = line.Split(',');
                    //write the headers line
                    writer.WriteLine(line);
                }

                //while the reader is not at the end of the file 
                while ((line = reader.ReadLine()) != null)
                {
                    //split the lines 
                    var columns = line.Split(',');
                    //if what is in the first column of the split line isn't equal to the booking number being looked for
                    if (columns[0] != customerRefNum.ToString())
                    {
                        //write the line to the temp file
                        writer.WriteLine(string.Join(delimiter, columns));                     
                    }
                    //else if what is in the first column is what is being looked for, delete the line by ignoring it in the writing of the new file
                    else 
                    {
                        bool deleted = false;
                        //check the bookings lists to check if the customer has bookings still
                        foreach (Booking b in lists.BookingsList)
                        {
                            if (customerRefNum == b.CustomerRefNumber)
                            {
                                deleted = false;
                                //keep the customer, don't delete it
                                writer.WriteLine(string.Join(delimiter, columns));
                                MessageBox.Show("This customer still has bookings and therefore can't be deleted.");
                                break;
                            }
                            else
                            {
                                deleted = true;
                            }
                        }
                        //update the list as well 
                        foreach (Customer c in lists.CustomersList)
                        {
                            //if the customer number given equals a customer reference number in the list
                            if (customerRefNum == c.CustomerRefNum && deleted)
                            {
                                //show a message showing the changes made
                                MessageBox.Show("Deleted: customer " + customerRefNum);
                                //remove the customer from the list 
                                lists.CustomersList.Remove(c);
                                deleted = false;
                                break;
                            }
                        }
                    }                                           
                }
            }
            //delete the old file 
            File.Delete(sourcePath);
            //move the new file from the temp path to the original path
            File.Move(tempPath, sourcePath);
        }
        #endregion
    }




     
}
