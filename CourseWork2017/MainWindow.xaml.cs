﻿/*
 * Author : Fionn Mahnig
 * Class purpose: Class that holds the code for the GUI of the main window (all the tabs) 
 * (creation of customers/guests/bookings, Amend customers/bookings/guests/extras, Delete bookings/customers)
 * Date last modified: 09/12/2016 
 * Part of: Facade design pattern
*/

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CourseWork2017
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //reference to facade
        private Facade facade = new Facade();
        //reference to the logger
        private Logger logger = Logger.Instance;
        //reference to the lists
        private Collections lists = Collections.Instance;

        //convert guest age and booking number to ints from textbox (in btnCreate_click)
        private int guestAge;
        private int bookingNumber;
        private int custRefNum;
        private int temp; //temporary int to try parse and check if int or not in the textboxes

        public MainWindow()
        {
            InitializeComponent();
            //fill in the lists at the start of the program, using the csv files
            facade.populateListsStartProgram();
        }


        #region NEW CUSTOMER
        private void btnCreate_Click(object sender, RoutedEventArgs e)
        {
            //if you are currently on the new customer tab
            if (tabNewCustomer.IsSelected)
            {
                try
                {
                    if (txtNewCustAddress.Text != "" && txtNewCustFName.Text != "" && txtNewCustLName.Text != "")
                    {
                        //add new person to the personlist
                        facade.AddNewPerson("Customer", bookingNumber, txtNewCustFName.Text, txtNewCustLName.Text, txtNewCustAddress.Text, txtNewGustPassNum.Text, guestAge);

                        //show the newly created customer's ref number if it has been created
                        if (facade.GetLastCustomerList() != null)
                            txtNewCustCustRefNum.Text = facade.GetLastCustomerList().CustomerRefNum.ToString();

                    }
                        //if one or all of the textboxes haven't been filled in, throw an error
                    else
                    {
                        throw new ArgumentException("First Name, Last Name and Address need to be filled in.");
                    }
                }
                //show the error from the argument exception if there is one
                catch (Exception excep)
                {
                    //show error message 
                    MessageBox.Show(excep.Message);
                }
            }
        #endregion

        #region NEW GUEST
            //else if you are on new guest tab
            else if (tabNewGuest.IsSelected)
            {
                try
                {                               
                    //check if a number has been entered in the age textbox
                    if (int.TryParse(txtNewGuestAge.Text, out temp))
                    {
                        //convert the age to int if filled in and numeric
                        guestAge = Convert.ToInt32(txtNewGuestAge.Text);
                        //guest age needs to be between 0 and 101
                        if (guestAge > 101 || guestAge < 0)
                        {
                            //if it's not, throw a new exception
                            throw new ArgumentException("Age needs to be between 0 and 101.");
                        }
                    }
                    else
                    {
                        //if not a number entered, throw an exception
                        throw new ArgumentException("Age needs to be filled in and numeric");
                    }


                    //check if a number has been entered in the booking number textbox
                    if (int.TryParse(txtNewGuestBookingRefNum.Text, out temp))
                        bookingNumber = Convert.ToInt32(txtNewGuestBookingRefNum.Text); //convert to int if filled in and numeric
                    else //if one or all of the textboxes haven't been filled in, throw an error
                        throw new ArgumentException("Passport number needs to be filled in and numeric");

                    //check if the booking exists with the findbooking method; if it doesn't return null, add a new guest
                    if (facade.FindBooking(bookingNumber) != null)
                    {
                        //add new person to the personlist
                        facade.AddNewPerson("Guest", bookingNumber, txtNewGuestFName.Text, txtNewGuestLName.Text, txtNewCustAddress.Text, txtNewGustPassNum.Text, guestAge);
                    }
                    else
                    {
                        //show a message informing that the booking doesn't exist
                        throw new ArgumentException("Booking doesn't exist.");
                    }
           
                }
                    //show the error from the argument exception if there is one
                catch (Exception excep)
                {
                    //show error message 
                    MessageBox.Show(excep.Message);
                }
            }
            #endregion

        #region NEW BOOKING
            //if pressing create on the booking tab
            else if (tabNewBooking.IsSelected)
            {
                try
                {
                    //check if a number has been entered in the customer ref num text box and if it's numeric 
                    if (int.TryParse(txtNewBookingCustRefNum.Text, out temp))
                    {
                        //convert from string to int if numeric 
                        custRefNum = Convert.ToInt32(txtNewBookingCustRefNum.Text);
                    }
                    else
                    {
                        //else show an error message
                        throw new ArgumentException("Customer reference number needs to be filled in and numeric.");
                    }
                    //check if the customer exists with the findcustomer method, and if it doesn't return null, add a new booking
                    if (facade.FindCustomer(custRefNum) != null)
                    {
                        //call the add booking method from the facade when clicking on create button
                        facade.AddNewBooking(custRefNum, datNewBookingArrDate.SelectedDate.Value.Date, datNewBookingDepDate.SelectedDate.Value.Date);
                    }
                    else
                    {
                        throw new ArgumentException("Customer doesn't exist.");
                    }
                    //show the newly created booking number 
                    if (facade.GetLastBooking() != null)
                        txtNewBookingBookingRefNum.Text = facade.GetLastBooking().BookingRefNumber.ToString();

                    //when creating a new booking, ask if the customer would like to add extras
                    if (MessageBox.Show("Add extras?", "Extras", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.No)
                    {
                        //if no, do nothing
                    }
                    else
                    {
                        //create a new extras window to open
                        ExtrasWindow extrasWindow = new ExtrasWindow();
                        //open the extra window
                        extrasWindow.Show();
                        //send the booking ref number 
                        extrasWindow.txtExtrasBookingNum.Text = txtNewBookingBookingRefNum.Text;
                    }
                }
                //show the error from the argument exception if there is one
                catch (Exception excep)
                {
                    //show error message 
                    MessageBox.Show(excep.Message);
                }
               

            }

        }
            #endregion

        #region CLEAR FORMS
        /// <summary>
        /// Clear the forms (new customer/guest/booking)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            //when on the customer tab
            if (tabNewCustomer.IsSelected)
            {
                //clear all the text boxes
                txtNewCustAddress.Clear();
                txtNewCustFName.Clear();
                txtNewCustLName.Clear();
            }
            //when on the guest tab
            else if (tabNewGuest.IsSelected)
            {
                //clear all the text boxes
                txtNewGuestAge.Clear();
                txtNewGuestBookingRefNum.Clear();
                txtNewGuestFName.Clear();
                txtNewGuestLName.Clear();
                txtNewGustPassNum.Clear();
            }
            //when on the booking tab
            else if (tabNewBooking.IsSelected)
            {
                //clear all the text boxes
                txtNewBookingCustRefNum.Clear();
            }
        }
        #endregion

        #region Amend Save button Customer/Guests/Booking/Extras
        private void btnAmendSave_Click(object sender, RoutedEventArgs e)
        {
            //what happens when you press the save button, depending on the selected tab
            if (tabAmendCustomer.IsSelected)
            {
                //when on amend customer tab
                logger.AmendCustomers(Convert.ToInt32(txtAmendCustCustRefNum.Text), txtAmendCustFName.Text, txtAmendCustLName.Text, txtAmendCustAddress.Text);
            }
           
            else if (tabAmendGuest.IsSelected)
            {
                //when on the amend guest tab
                logger.AmendGuests(Convert.ToInt32(txtAmendGuestBookingNum.Text), txtAmendGuestFName.Text, txtAmendGuestLName.Text, txtAmendGuestPassNum.Text, Convert.ToInt32(txtAmendGuestAge.Text));
               
            }
            else if (tabAmendBooking.IsSelected)
            {
                //when on the amend booking tab
                logger.AmendBookings(Convert.ToInt32(txtAmendBookingBookingRefNum.Text), datAmendBookingArrDate.SelectedDate.Value.Date, datAmendBookingDepDate.SelectedDate.Value.Date);
            }
            else if (tabAmendExtras.IsSelected)
            { 
                //when on the amend extras tab
                logger.AmendExtras(Convert.ToInt32(txtAmendExtrasBookingNum.Text), txtAmendExtrasBreakDietReq.Text, txtAmendExtrasEveReq.Text, datAmendExtrasCarHireStart.SelectedDate.Value.Date, datAmendExtrasCarHireEnd.SelectedDate.Value.Date, txtAmendExtrasDriverName.Text);

            }
        }

        #endregion

        #region AMEND CUSTOMER GET DETAILS FROM LIST
        /// <summary>
        /// Get the customer's details when you click on the button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAmendCustGetDetails_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                int customerReferenceNumber = 0;

                if (int.TryParse((txtAmendCustCustRefNum.Text), out temp))
                {
                    customerReferenceNumber = Convert.ToInt32(txtAmendCustCustRefNum.Text);
                }
                else
                {
                    throw new ArgumentException("Customer Reference Number needs to be filled in or numeric.");
                }
                //check the list of customers by getting a customer returned with the findcustomer method in facade
                Customer cust = facade.FindCustomer(customerReferenceNumber);

                //if a customer has been returned
                if (cust != null)
                {
                    //enable the different textboxes and fill them with the data
                    txtAmendCustAddress.IsEnabled = true;
                    txtAmendCustFName.IsEnabled = true;
                    txtAmendCustLName.IsEnabled = true;
                    txtAmendCustAddress.Text = cust.Address;
                    txtAmendCustFName.Text = cust.FirstName;
                    txtAmendCustLName.Text = cust.LastName;
                }
                else
                {
                    //else, if you get null back, and some fields had been enabled from a previous search, disable them and empty them of their content
                    txtAmendCustAddress.IsEnabled = false;
                    txtAmendCustFName.IsEnabled = false;
                    txtAmendCustLName.IsEnabled = false;
                    txtAmendCustAddress.Text = "";
                    txtAmendCustFName.Text = "";
                    txtAmendCustLName.Text = "";
                }
            }
            catch (Exception excep)
            {
                MessageBox.Show(excep.Message);
            }
        }
        #endregion

        #region AMEND GUESTS GET DETAILS FROM LIST AND CHANGE SELECTION COMBO BOX
        /// <summary>
        /// Get the details of the guests when clicking on get details, by filling in the combo box with all the guests linked to a booking
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAmendGuestGetDetails_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //empty the text boxes every time you press on get details
                txtAmendGuestAge.Text = "";
                txtAmendGuestFName.Text = "";
                txtAmendGuestPassNum.Text = "";
                txtAmendGuestLName.Text = "";

                //empty the combo box every time you press the get details button
                //while the list isn't empty
                while (cboAmendGuestGuestName.Items.Count != 0)
                {
                    //go through each line and remove it
                    cboAmendGuestGuestName.Items.RemoveAt(0);
                }

                //get the guests from the list by finding them by comparing the booking number entered to the ones stored in the guests list
                foreach (Guest g in lists.GuestsList)
                {
                    //check if the value input in the booking number text box is a number
                    if (int.TryParse(txtAmendGuestBookingNum.Text, out temp))
                    {
                        //if it is a number, convert it to an int
                        bookingNumber = Convert.ToInt32(txtAmendGuestBookingNum.Text);
                        //if that booking number equals a booking number in the guests list
                        if (bookingNumber == g.BookingNumber)
                        {
                            //enable the combo box
                            cboAmendGuestGuestName.IsEnabled = true;
                            //add all the guest to the combo box
                            cboAmendGuestGuestName.Items.Add(g.FirstName + " " + g.LastName);
                        }
                    }
                    else
                    {
                        //if the value in booking number is not a number or is empty, create an exception
                        throw new ArgumentException("Booking number needs to be filled in or numeric.");
                    }
                }
            }
            catch (Exception excep)
            {                   
                //show an error window if the booking number is in an incorrect value
                MessageBox.Show(excep.Message);
            }
        }

        /// <summary>
        /// this changes the different text boxes values in "Amend Guest" depending on the value chosen in the combo box
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cboAmendGuestGuestName_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //set the current selection
            var currentSelection = (sender as ComboBox).SelectedItem as string;

            //check the list of guests
            foreach (Guest g in lists.GuestsList)
            {
                //if the current selection equals anything in the list with same first name and last name
                if (currentSelection == g.FirstName + " " + g.LastName)
                {
                    //enable the different boxes
                    txtAmendGuestFName.IsEnabled = true;
                    txtAmendGuestLName.IsEnabled = true;
                    txtAmendGuestAge.IsEnabled = true;
                    txtAmendGuestPassNum.IsEnabled = true;

                    //set the different values with what's in the list for this guest
                    txtAmendGuestFName.Text = g.FirstName;
                    txtAmendGuestLName.Text = g.LastName;
                    txtAmendGuestAge.Text = g.Age.ToString();
                    txtAmendGuestPassNum.Text = g.PassportNum;
                }

            }
        }
        #endregion

        #region AMEND EXTRAS GET DETAILS FROM LIST

        /// <summary>
        /// When you press on the get details button on the amend extras tab, call the find extras method and fill in the boxes with the found data 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAmendExtrasGetDetails_Click(object sender, RoutedEventArgs e)
        {
            
            //disable all the boxes when looking for a new number to activate only the ones needed
            chkAmendExtrasBreakfast.IsEnabled = false;
            chkAmendExtrasEveMeal.IsEnabled = false;
            chkAmendExtrasCarHire.IsEnabled = false;
            chkAmendExtrasBreakfast.IsChecked = false;
            txtAmendExtrasBreakDietReq.IsEnabled = false;
            chkAmendExtrasEveMeal.IsChecked = false;
            txtAmendExtrasEveReq.IsEnabled = false;
            chkAmendExtrasCarHire.IsChecked = false;
            datAmendExtrasCarHireStart.IsEnabled = false;
            datAmendExtrasCarHireEnd.IsEnabled = false;
            txtAmendExtrasDriverName.IsEnabled = false;

            try
            {
                //check if the value input in booking number is a number
                if (int.TryParse(txtAmendExtrasBookingNum.Text, out temp))
                {
                    //if it is, convert it to an int
                    bookingNumber = Convert.ToInt32(txtAmendExtrasBookingNum.Text);
                }
                else
                {
                    //else throw an exception
                    throw new ArgumentException("Booking number needs to be filled in or numeric.");
                }
                //create a new extra with info gotten with the method to find the right extra (same booking number as input)
                Extras extra = facade.FindExtras(bookingNumber);

                //if the extra returned is not null
                if (extra != null)
                {
                    //enable the different boxes depending on what the extras in the list already have
                    //if the breakfast requirement is not null
                    if (extra.BreakDietRequirements != "")
                    {
                        //enable the boxes and fill them in with the data we got from the list, above
                        chkAmendExtrasBreakfast.IsChecked = true;
                        chkAmendExtrasBreakfast.IsEnabled = true;
                        txtAmendExtrasBreakDietReq.IsEnabled = true;
                        txtAmendExtrasBreakDietReq.Text = extra.BreakDietRequirements;
                    }
                    //if the evening meal requirements is no null
                    if (extra.EveDietRequirements != "")
                    {
                        //enable the boxes and fill them in with the data we got from the list, above
                        chkAmendExtrasEveMeal.IsChecked = true;
                        chkAmendExtrasEveMeal.IsEnabled = true;
                        txtAmendExtrasEveReq.IsEnabled = true;
                        txtAmendExtrasEveReq.Text = extra.EveDietRequirements;
                    }
                    // if the hire start date doesn't have the default value, fill in the different values with the data we got from the list, above
                    if (extra.HireStartDate != default(DateTime))
                    {
                        //enable the boxes and fill them in with the data we got from the list, above
                        chkAmendExtrasCarHire.IsEnabled = true;
                        chkAmendExtrasCarHire.IsChecked = true;
                        datAmendExtrasCarHireStart.IsEnabled = true;
                        datAmendExtrasCarHireStart.SelectedDate = extra.HireStartDate;
                        datAmendExtrasCarHireEnd.IsEnabled = true;
                        datAmendExtrasCarHireEnd.SelectedDate = extra.HireEndDate;
                        txtAmendExtrasDriverName.IsEnabled = true;
                        txtAmendExtrasDriverName.Text = extra.NameDriver;
                    }
                }
            }
            catch (Exception excep)
            {
                //show the exception message if  the value input is not a number
                MessageBox.Show(excep.Message);
            }
        }
        #endregion

        #region AMEND BOOKING GET DETAILS FROM LIST
        /// <summary>
        /// pressing this button will fill in the boxes in the amend booking tab if the booking reference number exists
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAmendBookingGetDetails_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //check if the value input in booking number textbox is a number
                if (int.TryParse(txtAmendBookingBookingRefNum.Text, out temp))
                {
                    //if it is, convert it to int 
                    bookingNumber = Convert.ToInt32(txtAmendBookingBookingRefNum.Text);
                }
                else
                {
                    //else create an exception
                    throw new ArgumentException("Booking number needs to be filled in or numeric.");
                }
             
                //check the list of bookings by getting a booking returned with the finbooking method in facade
                Booking book = facade.FindBooking(bookingNumber);

                //if the method didn't return null
                if (book != null)
                {
                    //enable the different boxes
                    datAmendBookingArrDate.IsEnabled = true;
                    datAmendBookingDepDate.IsEnabled = true;
                    //fill in the boxes with the values from the list
                    datAmendBookingArrDate.SelectedDate = book.ArrivalDate;
                    datAmendBookingDepDate.SelectedDate = book.DepartureDate;
                }
                else
                {
                    //else, disable the boxes if the value returned is null
                    datAmendBookingArrDate.IsEnabled = false;
                    datAmendBookingDepDate.IsEnabled = false;
                }
            }
            catch (Exception excep)
            {
                //if the value input in booking number textbox is not a number, show the exception
                MessageBox.Show(excep.Message);
            }
        }
        #endregion

        #region DELETE
        /// <summary>
        /// Delete a booking by inputting a booking number and pressing on the delete button on the delete tab
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDeleteBooking_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //check if the input value in booking number text box is a number 
                if (int.TryParse(txtDeleteBooking.Text, out temp))
                {
                    //if it is, convert it to an int
                    bookingNumber = Convert.ToInt32(txtDeleteBooking.Text);
                }
                else
                {
                    //else, throw an exception
                    throw new ArgumentException("Booking number needs to be filled in or numeric.");
                }
                //delete the booking with booking number specified
                logger.DeleteBooking(bookingNumber);
            }
            catch (Exception excep)
            {
                //if the value input is not a number, show the exception message
                MessageBox.Show(excep.Message);
            }
        }

        /// <summary>
        /// Delete a customer by inputting a booking number and pressing on the delete button on the delete tab. 
        /// If the customer still has bookings, the customer won't get deleted.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDeleteCustomer_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //check if the input value in customer number text box is a number 
                if (int.TryParse(txtDeleteCustomer.Text, out temp))
                {
                    //if it is, convert it to an int
                    custRefNum = Convert.ToInt32(txtDeleteCustomer.Text);
                }
                else
                {
                    //else, throw an exception
                    throw new ArgumentException("Customer reference number needs to be filled in or numeric.");
                }
                //delete the customer with customer number specified
                logger.DeleteCustomer(custRefNum);
            }
            catch (Exception excep)
            {
                //if the value input is not a number, show the exception message
                MessageBox.Show(excep.Message);
            }
        }

        #endregion


        #region hide show stuff amend extras
        private void chkAmendExtrasEveMeal_Click(object sender, RoutedEventArgs e)
        {
            if (txtAmendExtrasEveReq.IsEnabled == true)
            {
                txtAmendExtrasEveReq.IsEnabled = false;
            }
            else
                txtAmendExtrasEveReq.IsEnabled = true;
        }

        private void chkAmendExtrasBreakfast_Click(object sender, RoutedEventArgs e)
        {
            if (txtAmendExtrasBreakDietReq.IsEnabled == true)
                txtAmendExtrasBreakDietReq.IsEnabled = false;
            else
                txtAmendExtrasBreakDietReq.IsEnabled = true;
        }

        private void chkAmendExtrasCarHire_Click(object sender, RoutedEventArgs e)
        {
            if (datAmendExtrasCarHireEnd.IsEnabled == true && datAmendExtrasCarHireStart.IsEnabled == true && txtAmendExtrasDriverName.IsEnabled == true)
            {
                datAmendExtrasCarHireStart.IsEnabled = false;
                datAmendExtrasCarHireEnd.IsEnabled = false;
                txtAmendExtrasDriverName.IsEnabled = false;
            }
            else
            {
                datAmendExtrasCarHireStart.IsEnabled = true;
                datAmendExtrasCarHireEnd.IsEnabled = true;
                txtAmendExtrasDriverName.IsEnabled = true;
            }
        }


#endregion

     
    }
}
