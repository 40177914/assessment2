﻿/*
 * Author : Fionn Mahnig
 * Class purpose: Holds the different lists and methods to add new items to the lists
 * Date last modified: 09/12/2016 
 * Part of: Facade design patterns
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace CourseWork2017
{
    /// <summary>
    /// Class to hold the different lists and their add methods
    /// </summary>
    class Collections
    {
        //lists. Bookings, customers, guests, extras
        private List<Booking> bookingsList = new List<Booking>();
        private List<Customer> customersList = new List<Customer>();
        private List<Guest> guestsList = new List<Guest>();
        private List<Extras> extrasList = new List<Extras>();

        //make the collections a singleton to make sure to always access the same lists
        private static Collections instance;

        private Collections()
        { }

        //make a unique instance of collections
        public static Collections Instance
        {
            /* This property is used to access the only instance of Collections. It is static in
            it to be called without instanciating an object.
            It returns an object that represents the only instance of the Collections class*/
            get
            {
                if (instance == null)
                {
                    instance = new Collections();
                }
                return instance;
            }
        }

        #region accessors

        //list of extras
        public List<Extras> ExtrasList
        {
            get { return extrasList; }
            set { extrasList = value; }
        }

      
        /// <summary>
        /// list of bookings
        /// </summary>
        public List<Booking> BookingsList
        {
            get { return bookingsList; }
            set { bookingsList = value; }
        }
        

        /// <summary>
        /// list of customers
        /// </summary>
        public List<Customer> CustomersList
        {
            get { return customersList; }
            set { customersList = value; }
        }

        /// <summary>
        /// list of guests
        /// </summary>
        public List<Guest> GuestsList
        {
            get { return guestsList; }
            set { guestsList = value; }
        }

        #endregion

        #region METHOD TO ADD TO LISTS
        /// <summary>
        /// method to add something to the guest list from other classes
        /// </summary>
        /// <param name="guest">guest to be added</param>
        public void AddToGuestsList(Guest guest)
        {
            guestsList.Add(guest);
        }

        /// <summary>
        /// method to add something to the booking list from other classes
        /// </summary>
        /// <param name="booking">booking to be added</param>
        public void AddToBookingsList(Booking booking)
        {
            bookingsList.Add(booking);
        }
        /// <summary>
        /// method to add something to the extras list from other classes
        /// </summary>
        /// <param name="extra">extras to be added</param>
        public void AddToExtrasList(Extras extra)
        {
            extrasList.Add(extra);
        }
        /// <summary>
        /// method to add something to the customers list from other classes
        /// </summary>
        /// <param name="customer">customer to be added</param>
        public void AddToCustomersList(Customer customer)
        {
            //if(customer.FirstName !="" && customer.LastName != "" && customer.Address != "")
            customersList.Add(customer);
            //else 
            //    MessageBox.Show("You need to fill in First Name, Last Name and Address.");
        }
        #endregion

    }
}
