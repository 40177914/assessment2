﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CourseWork2017
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //reference to facade
        private Facade facade = new Facade();
        public MainWindow()
        {
            InitializeComponent();        
        }

        private void btnNewCustCreate_Click(object sender, RoutedEventArgs e)
        {
            //add new person to the personlist
            facade.AddNewPerson(txtNewCustFName.Text, txtNewCustLName.Text, txtNewCustAddress.Text);

            //show the customer reference number from the last customer created in the ref box           
            txtNewCustCustRefNum.Text = facade.PersonList.Last().ToString();

            //debug for name
            txtShowList.Text = facade.PersonList.Last().Name.ToString();
            
        }

        private void btnNewBookingCreate_Click(object sender, RoutedEventArgs e)
        {
            //convert from string to int 
            int custrefnum = Convert.ToInt32(txtNewBookingCustRefNum.Text);

            //call the add booking method from the facade when clicking on create button
            facade.AddNewBooking(custrefnum, datNewBookingArrDate.DisplayDate, datNewBookingDepDate.DisplayDate);

            //debug, show last booking's arr date
            txtnewbookingtestblock.Text = facade.BookingsList.Last().ArrivalDate.ToString();

            //Booking newBooking = refNumbers.BookingFact("Booking");
            //bookingRef++; //increment the reference num
            //new Booking(bookingRef); //create a new booking
        }



    }
}
