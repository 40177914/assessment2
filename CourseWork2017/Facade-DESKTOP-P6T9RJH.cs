﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace CourseWork2017
{
    class Facade
    {
        //reference to the factory
        private Factory refNumFactory = new Factory();
        private List<Person> personList = new List<Person>();
        private List<Booking> bookingsList = new List<Booking>();

        /// <summary>
        /// accessors for list of bookings
        /// </summary>
        internal List<Booking> BookingsList
        {
            get { return bookingsList; }
            set { bookingsList = value; }
        }
        /// <summary>
        /// accessor for list of customers and guest
        /// </summary>
        internal List<Person> PersonList
        {
            get { return personList; }
            set { personList = value; }
        }

        /// <summary>
        /// add a new person to the system (list)
        /// </summary>
        /// <param name="fName">first name</param>
        /// <param name="lName">last name</param>
        /// <param name="address">address</param>
        public void AddNewPerson(string fName, string lName, string address)
        {
            //if the boxes are not empty in new customer
            if (fName != "" && lName != "" && address != "")
            {
                //create a new customer with the factory
                Person newCustomer = refNumFactory.PersonFact("Customer", address, fName, lName);
                newCustomer.Name = fName + " " + lName;             

                //add that new person to the list
                personList.Add(newCustomer);                         

            }
            else
            {
                MessageBox.Show("You need to fill in Last Name, First Name and Address.");               
            }
        }

        /// <summary>
        /// add a new booking to the system
        /// </summary>
        /// <param name="custRefNumber">customer reference number, to add a booking to a customer (assume that a booking needs to be tied to a customer)</param>
        /// <param name="arrivalDate">customer's arrival date</param>
        /// <param name="departureDate">customer's departure date</param>
        public void AddNewBooking(int custRefNumber, DateTime arrivalDate, DateTime departureDate)
        {
            //create a new booking with the factory
            Booking newBooking = refNumFactory.BookingFact("Booking");
            //set the arrival date via the date box via the mainwindow
            newBooking.ArrivalDate = arrivalDate;
            //set the departure date via the date box via the mainwindow
            newBooking.DepartureDate = departureDate;

            //add the new booking to the list
            bookingsList.Add(newBooking);
        }

    
}
}
