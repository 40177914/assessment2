﻿/*
 * Author : Fionn Mahnig
 * Class purpose: This class will create Persons (Customers or Guests) depending on the type received from the facade. It will also use 
 * the details received to save them in a csv file with the logger
 * Date last modified: 09/12/2016 
 * Part of: Facade design pattern
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace CourseWork2017
{

    class Factory
    {
        private int custRef = 0; //to be able to increment the customer reference num
        private int bookingRef = 0;
        
       // private Facade facade = new Facade();
        private Logger logger = Logger.Instance; //create a logger object to use it and log the customers, guests and bookings to a file

        #region PERSON FACTORY(guest + customer)
        /// <summary>
        /// Creates a customer or a guest with different parameters, depending on the type received from the facade, and will also log them to a file as well as adding them to a list
        /// </summary>
        /// <param name="isFile">will be true or false depending on if the new person is written from file into list or from program into list</param>
        /// <param name="type">type of person (customer or guest)</param>
        /// <param name="custRefNum">customer reference number</param>
        /// <param name="bookingNumber">booking number</param>
        /// <param name="fName">first name</param>
        /// <param name="lName">last name</param>
        /// <param name="address">address</param>
        /// <param name="passportNum">passport number</param>
        /// <param name="age">age</param>
        /// <returns>returns new customer or new guest to the facade</returns>
        public Person PersonFact(bool isFile, string type, int custRefNum, int bookingNumber, string fName, string lName, string address, string passportNum, int age)
        {
            if (type == "Customer")
            {
                try
                {
                    //customer reference number is the latest number from the list, calculate in facade
                    custRef = custRefNum;
                    //isfile will be true if coming from the logger and false if coming from the facade, 
                    //so that it doesn't add 1 to the cust ref num when you load the data in
                    if (!isFile)
                    {                       
                        //if true, add 1 to the latest cust ref num
                        custRef++;
                    }
                    Customer cust = new Customer(type, custRef, bookingNumber, fName, lName, address, passportNum, age); //create a new customer if the type received is customer   
                    cust.CustomerRefNum = custRef;  //set the customer reference number
                    cust.Address = address; //set the customer address
                    cust.FirstName = fName; //set the customer name
                    cust.LastName = lName;
                    //if isFile false, won't be copied again in the csv file 
                    if (!isFile)
                        logger.logCustomer(cust.CustomerRefNum, cust.FirstName, cust.LastName, cust.Address); // log the new customer to a file                        
                    return cust; //return the customer to the facade 
                }
                catch (Exception excep)
                {
                    //show error message 
                    MessageBox.Show(excep.Message);
                    return null;
                }

            }
            if (type == "Guest")
            {
                try
                {
                    //create a new guest
                    Guest guest = new Guest(type, bookingNumber, fName, lName, passportNum, age);
                    guest.BookingNumber = bookingNumber; //set the guest number to the booking number to link the guests with the booking they belong to
                    guest.PassportNum = passportNum; //guest's passport number
                    guest.Age = age; //guest's age
                    guest.FirstName = fName; //guest's name (fname + lname)
                    guest.LastName = lName;
                    //if false, won't be copied again in the csv file 
                    if (!isFile)
                        logger.logGuest(guest.BookingNumber, guest.FirstName, guest.LastName, guest.PassportNum, guest.Age); //log a new guest to a file
                    return guest; //return the new guest to the facade
                }
                catch (Exception excep)
                {
                    //show error message 
                    MessageBox.Show(excep.Message);
                    return null;
                }
            }
            else
                return null;
        }
        #endregion

        #region BOOKING FACTORY
        /// <summary>
   /// Create a booking and add it to a list and log it to a csv file
   /// </summary>
   /// <param name="isFile">will be true or false depending on if the new person is written from file into list or from program into list</param>
   /// <param name="type">type(booking)</param>
   /// <param name="bookingRefNum">booking reference number</param>
   /// <param name="customerRefNum"> customer reference number</param>
   /// <param name="arrivalDate">arrival date</param>
   /// <param name="departureDate">departure date</param>
   /// <returns>returns a new booking to the facade</returns>
        public Booking BookingFact(bool isFile, string type, int bookingRefNum,  int customerRefNum, DateTime arrivalDate, DateTime departureDate)
        {
            if (type == "Booking")
            {
                //set the booking ref number
                bookingRef = bookingRefNum;

                //isfile will be true if coming from the logger and false if coming from the facade, 
                //so that it doesn't add 1 to the booking ref num when you load the data in
                if (!isFile)
                  bookingRef++;
                //create a new booking
                Booking booking = new Booking(bookingRef, customerRefNum, arrivalDate, departureDate);
                //set the details of the booking
                booking.BookingRefNumber = bookingRef; //booking number 
                booking.CustomerRefNumber = customerRefNum; //customer reference number 
                booking.ArrivalDate = arrivalDate; //arrival date
                booking.DepartureDate = departureDate; //departure date
                //if false, won't be copied again in the csv 
                if(!isFile)
                    logger.logBookings(booking.BookingRefNumber, booking.CustomerRefNumber, booking.ArrivalDate, booking.DepartureDate);
                //return the booking to the facade
                return booking;
            }
            else
                throw new ArgumentException();
        }
        #endregion

    }
}
