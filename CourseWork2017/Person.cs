﻿/*
 * Author : Fionn Mahnig
 * Class purpose: Superclass, guest inherits from this. Defines what 
 * Date last modified: 09/12/2016 
 * Part of: Factory design pattern
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace CourseWork2017
{
    abstract class Person
    {
        //type is the type that Person is going to get (Customer or Guest)
        private string type;
        //first name and last name of guest and customer
        protected string firstName, lastName;

        #region ACCESSORS

        public string Type
        {
            get { return type; }
            set { type = value; }
        }   

        //store the first name. If the first name textbox is empty, a message will ask to first enter data
        public string FirstName
        {
            get { return firstName; }
            set
            {
                //if the value in first name is empty
                if (value == "")
                {
                    //send an error message
                    throw new ArgumentException("Last Name, First Name and Address need to be filled in."); 
                }
                //if value is valid
                firstName = value; 
            }
        }

        //store the last name. If the last name textbox is empty, a message will ask to first enter data
        public string LastName
        {
            get { return lastName; }
            set 
            {
                //if the value in last name is empty
                if (value == "")
                {
                    //give an error message
                    throw new ArgumentException("Last Name, First Name and Address need to be filled in.");
                }
                //if value is valid
                lastName = value; 
            }
        }

        #endregion

        public abstract void print();
    }
}
