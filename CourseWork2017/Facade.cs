﻿/*
 * Author : Fionn Mahnig
 * Class purpose: This class makes it so that the GUI is not cluttered with too much code and the necessary methods will be here and only called from other classes.
 * Date last modified: 09/12/2016 
 * Part of: Factory design patterns
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.IO;

namespace CourseWork2017
{
    class Facade
    {
        //reference to the only instance of collections
        private Collections lists = Collections.Instance;
        //reference to the factory
        private Factory refNumFactory = new Factory();

        #region ADD NEW PERSON/GUEST / BOOKING / EXTRAS
       /// <summary>
       /// Add a new person to the system (list). Can be a customer or guest depending on the type
       /// </summary>
       /// <param name="type">tye: customer or guest</param>
       /// <param name="bookingNumber">booking number</param>
       /// <param name="fName">first name </param>
       /// <param name="lName">last name</param>
       /// <param name="address">address</param>
       /// <param name="passportNum">passport number</param>
       /// <param name="age">age</param>
        public void AddNewPerson(string type, int bookingNumber, string fName, string lName, string address, string passportNum, int age)
        {
            int custRefNum;
            //get the latest booking ref number from the list  
            if (lists.CustomersList.Count > 0)
                custRefNum = lists.CustomersList.Last().CustomerRefNum;
            else
                custRefNum = 0;
            
            //create a new person with the factory        
            Person newCustomer = refNumFactory.PersonFact(false, type, custRefNum, bookingNumber, fName, lName, address, passportNum, age);

            if (type == "Customer")
            {
                //add that new person to the list
                lists.AddToCustomersList(newCustomer as Customer);
            }
            else if (type == "Guest")
            {
                //create a new guest with the factory
                lists.AddToGuestsList(newCustomer as Guest);
                //feedback from pressing on the button to show that a new guest has been added
                MessageBox.Show("New guest added: " + newCustomer.FirstName);
            }
        }

        /// <summary>
        /// add a new booking to the system
        /// </summary>
        /// <param name="custRefNumber">customer reference number, to add a booking to a customer (assume that a booking needs to be tied to a customer)</param>
        /// <param name="arrivalDate">customer's arrival date</param>
        /// <param name="departureDate">customer's departure date</param>
        public void AddNewBooking(int custRefNumber, DateTime arrivalDate, DateTime departureDate)
        {
            int bookingRefNum;
            //get the latest booking ref number from the list if not empty
            if (lists.BookingsList.Count > 0)
                bookingRefNum = lists.BookingsList.Last().BookingRefNumber;
            else
                bookingRefNum = 0;
            
            //create a new booking with the factory
            Booking newBooking = refNumFactory.BookingFact(false, "Booking", bookingRefNum, custRefNumber, arrivalDate, departureDate);

            //add the new booking to the list
            lists.AddToBookingsList(newBooking);
        }

        /// <summary>
        /// if extras are selected during a booking, add them to a list, all the parameters, apart from booking number, are optional
        /// </summary>
        /// <param name="bookingNum">Booking number</param>
        /// <param name="breakDietReq">optional breakfast dietary requirements</param>
        /// <param name="eveDietReq">optional evening meal dietart requirements</param>
        /// <param name="driverName">optional driver name</param>
        /// <param name="hireStart">optional car hire start date</param>
        /// <param name="hireEnd">optional car hire end date</param>
        public void AddNewExtras(int bookingNum, string breakDietReq = "", string eveDietReq = "", string driverName ="", DateTime hireStart = default(DateTime), DateTime hireEnd = default(DateTime))
        {
            //create a new extra
            Extras extra = new Extras(bookingNum, breakDietReq, eveDietReq, driverName, hireStart, hireEnd);

            //add the new extra to the list
            lists.AddToExtrasList(extra);
        }

        #endregion

        #region POPULATE LISTS AT START PROGRAM
        /// <summary>
        /// populate the different lists (customers, guests, bookings) with data from the csv files at the start of the program
        /// </summary>
        public void populateListsStartProgram()
        {
            
            //if the log file exists
            if (File.Exists("LogCustomers.csv"))
            {
                //foreach lines in the csv file (skipping the first line which is headers)
                foreach (var line in File.ReadAllLines("LogCustomers.csv").Skip(1))
                {
                    //separate the values at the commas
                    string[] values = line.Split(',');
                    //create a new empty customer
                    Person cust = refNumFactory.PersonFact(true, "Customer", Convert.ToInt32(values[0]), 0, values[1], values[2], values[3], null, 0);
                    //fill cust with the values from the csv, separated by the commas and put in an array above
                    lists.CustomersList.Add(cust as Customer);
                }
            }
            

            //if the log file exists
            if (File.Exists("LogGuests.csv"))
            {
                foreach (var line in File.ReadAllLines("LogGuests.csv").Skip(1))
                {
                    //separate the values at the commas
                    string[] values = line.Split(',');
                    //create a new guest                   
                    Person guest = refNumFactory.PersonFact(true, "Guest", 0, Convert.ToInt32(values[0]), values[1], values[2], null, values[3], Convert.ToInt32(values[4]));
                    //add the newly created existing guest to the list
                    lists.GuestsList.Add(guest as Guest);
                }
            }
           

            //if the log file exists
            if (File.Exists("LogBookings.csv"))
            {
                foreach (var line in File.ReadAllLines("LogBookings.csv").Skip(1))
                {

                    //separate the values at the commas
                    string[] values = line.Split(',');
                    //create a new booking
                    Booking booking = refNumFactory.BookingFact(true, "Booking", Convert.ToInt32(values[0]),Convert.ToInt32(values[1]), Convert.ToDateTime(values[2]), Convert.ToDateTime(values[3]));
                    //add the newly created existing booking to the list
                    lists.BookingsList.Add(booking);
                }
            }

            //if the log file exists
            if (File.Exists("LogExtras.csv"))
            {
                foreach (var line in File.ReadAllLines("LogExtras.csv").Skip(1))
                {

                    //separate the values at the commas
                    string[] values = line.Split(',');
                    //create a new booking
                    Extras extra = new Extras(Convert.ToInt32(values[0]), values[1], values[2], values[5], Convert.ToDateTime(values[3]), Convert.ToDateTime(values[4]));
                    //add the newly created existing booking to the list
                    lists.ExtrasList.Add(extra);
                }
            }
        }

        #endregion 

        #region FIND CUSTOMERS / BOOKINGS / EXTRAS METHODS

        /// <summary>
        /// find a customer by checking the reference numbers and once found, return the customer
        /// </summary>
        /// <param name="custRefNum">reference number being looked for (from customer ref number box)</param>
        /// <returns>returns a customer with that customer ref number or nothing</returns>
        public Customer FindCustomer(int custRefNum)
        {
            //for loop to check through the list
            for (int i = 0; i < lists.CustomersList.Count; i++)
            {
                //if the ref number in the list is the same as the one giver as argument
                if (lists.CustomersList[i].CustomerRefNum == custRefNum)
                {
                    //return the customer associated with that ref number
                    return lists.CustomersList[i];
                }
                else
                {
                    //else continue through the list
                    continue;
                }
            }
            //if customer hasn't been found, show error
            MessageBox.Show("No customer associated with that reference number.");
            return null;
        }

        /// <summary>
        /// returns details of a booking by checking the list of bookings  and comparing the booking num given to the ones in the list
        /// </summary>
        /// <param name="bookingNum"></param>
        /// <returns></returns>
        public Booking FindBooking(int bookingNum)
        {
            //for loop to check through the list
            for (int i = 0; i < lists.BookingsList.Count; i++)
            {
                //if the ref number in the list is the same as the one giver as argument
                if (lists.BookingsList[i].BookingRefNumber == bookingNum)
                {
                    //return the booking associated with that ref number
                    return lists.BookingsList[i];
                }
                else
                {
                    //else continue through the list
                    continue;
                }
            }
            //if booking hasn't been found, show error
            MessageBox.Show("No bookings associated with that reference number.");
            return null;
        }

        /// <summary>
        /// returns details of extras by checking the list of extras and comparing the booking num given to the ones in the list
        /// </summary>
        /// <param name="bookingNum">booking number to be checked for</param>
        /// <returns></returns>
        public Extras FindExtras(int bookingNum)
        {
            //for loop to check through the list
            for (int i = 0; i < lists.ExtrasList.Count; i++)
            {
                //if the ref number in the list is the same as the one giver as argument
                if (lists.ExtrasList[i].BookingNum == bookingNum)
                {
                    //return the extra associated with that ref number
                    return lists.ExtrasList[i];
                }
                else
                {
                    //else continue through the list
                    continue;
                }
            }
            //if extra hasn't been found, show error
            MessageBox.Show("No extras associated with that reference number.");
            return null;
        }
        #endregion

        #region methods to get last data from lists 
        /// <summary>
        /// get the last customer from the customers list
        /// </summary>
        /// <returns>last customer</returns>
        public Customer GetLastCustomerList()
        {
            //if the list is not empty
            if (lists.CustomersList.Count > 0)
                return lists.CustomersList.Last();
            else
                return null;
        }

        /// <summary>
        /// get the last guest from the guests list
        /// </summary>
        /// <returns>last guest</returns>
        public Guest GetLastGuest()
        {
            //if the list is not empty
            if (lists.GuestsList.Count > 0)
                return lists.GuestsList.Last();
            else
                return null;
        }

        /// <summary>
        /// get the last booking from the bookings list
        /// </summary>
        /// <returns>last booking</returns>
        public Booking GetLastBooking()
        {
            //if the list is not empty
            if (lists.BookingsList.Count > 0)
                return lists.BookingsList.Last();
            else
                return null;
        }
        /// <summary>
        /// get the last extra from the extras list
        /// </summary>
        /// <returns>last extra</returns>
        public Extras GetLastExtras()
        {
            //if the list is not empty
            if (lists.ExtrasList.Count > 0)
                return lists.ExtrasList.Last();
            else
                return null;
            }
        #endregion 

    }
}